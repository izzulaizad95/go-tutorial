package main

import (
	"fmt"
	"log"

	"example.com/greetings"
	"golang.org/x/example/stringutil"
	"rsc.io/quote"
)

func main() {
	fmt.Println("Print quote: ", quote.Go())

	// Set properties of the predefined Logger, including
	// the log entry prefix and a flag to disable printing
	// the time, source file, and line number.
	log.SetPrefix("greetings: ")
	log.SetFlags(0)

	// Request a greeting message.
	message, err := greetings.Hello("Izzul")
	// If an error was returned, print it to the console and
	// exit the program.
	if err != nil {
		log.Fatal(err)
	}

	// If no error was returned, print the returned message
	// to the console.
	fmt.Println("Print greeting: ", message)

	// A slice of names.
	names := []string{"Gladys", "Samantha", "Darrin"}

	// Request greeting messages for the names.
	messages, err2 := greetings.Hellos(names)
	if err2 != nil {
		log.Fatal(err2)
	}

	// If no error was returned, print the returned map of
	// messages to the console.
	fmt.Println("Print multiple greetings: ", messages)

	// Reverse string
	fmt.Println("Print reverse of string: ", stringutil.Reverse("Hello"))

	// Upper case string
	fmt.Println("Print upper case of string: ", stringutil.ToUpper("Hello"))
}
